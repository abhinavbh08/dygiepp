# DyGIE++

Implements the model described in the paper [Entity, Relation, and Event Extraction with Contextualized Span Representations](https://www.semanticscholar.org/paper/Entity%2C-Relation%2C-and-Event-Extraction-with-Span-Wadden-Wennberg/fac2368c2ec81ef82fd168d49a0def2f8d1ec7d8).

This repository is under construction and we're in the process of adding support for more datasets.

## Table of Contents
- [Dependencies](#dependencies)
- [Model training](#training-a-model)
- [Model evaluation](#evaluating-a-model)
- [Pretrained models](#pretrained-models)
- [Making predictions](#making-predictions)


## Dependencies

This code was developed using Python 3.7. To create a new Conda environment using Python 3.7, do `conda create --name dygiepp python=3.7`.

The necessary dependencies can be installed with `pip install -r requirements.txt`.

The only dependencies for the modeling code are [AllenNLP](https://allennlp.org/) 0.9.0 and [PyTorch](https://pytorch.org/) 1.2.0. It may run with newer versions, but this is not guarenteed. For PyTorch GPU support, follow the instructions on the [PyTorch](https://pytorch.org/).

For data preprocessing a few additional data and string processing libraries are required including, [Pandas](https://pandas.pydata.org) and [Beautiful Soup 4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).

Finally, you'll need SciBERT for the scientific datasets. Run `python scripts/pretrained/get_scibert.py` to download and extract the SciBERT model to `./pretrained`.

## Training a model

### SciERC

To train a model for named entity recognition, relation extraction, and coreference resolution on the SciERC dataset:

- **Download the data**. From the top-level folder for this repo, enter `bash ./scripts/data/get_scierc.sh`. This will download the scierc dataset into a folder `./data/scierc`
- **Train the model**. Enter `bash ./scripts/train/train_scierc.sh [gpu-id]`. The `gpu-id` should be an integer like `1`, or `-1` to train on CPU. The program will train a model and save a model at `./models/scierc`.
- To train a "lightweight" version of the model that doesn't do coreference propagation, do `bash ./scripts/train/train_scierc_lightweight.sh [gpu-id]` instead. The result will go in `./models/scierc-lightweight`. More info on why you'd want to do this in the section on [making predictions](#making-predictions).




## Evaluating a model

To check the performance of one of your models or a pretrained model, you can use the `allennlp evaluate` command.

Note that `allennlp` commands will only be able to discover the code in this package if:
- You run the commands from the root folder of this project, `dygiepp`, or:
- You add the code to your Python path by running `conda develop .` from the root folder of this project.

Otherwise, you will get an error `ModuleNotFoundError: No module named 'dygie'`.

In general, you can make evaluate a model like this:
```shell
allennlp evaluate \
  [model-file] \
  [data-path] \
  --cuda-device [cuda-device] \
  --include-package dygie \
  --output-file [output-file] # Optional; if not given, prints metrics to console.
```
For example, to evaluate the [pretrained SciERC model](#pretrained-models), you could do
```shell
allennlp evaluate \
  pretrained/scierc.tar.gz \
  data/scierc/processed_data/json/test.json \
  --cuda-device 2 \
  --include-package dygie
```
To evaluate a model you trained on the SciERC data, you could do
```shell
allennlp evaluate \
  models/scierc/model.tar.gz \
  data/scierc/processed_data/json/test.json \
  --cuda-device 2  \
  --include-package dygie \
  --output-file models/scierc/metrics_test.json
```

## Making predictions

To make a prediction, you can use `allennlp predict`. For example, to make a prediction with the pretrained scierc model, you can do:

```
allennlp predict pretrained/scierc.tar.gz \
    data/scierc/processed_data/json/test.json \
    --predictor dygie \
    --include-package dygie \
    --use-dataset-reader \
    --output-file predictions/scierc-test.jsonl \
    --cuda-device 0
```

**Caveat**: Models trained to predict coreference clusters need to make predictions on a whole document at once. This can cause memory issues. To get around this there are two options:

- Make predictions using a model that doesn't do coreference propagation. These models predict a sentence at a time, and shouldn't run into memory issues. For SciERC, use `scierc-lightweight`. To train your own coref-free model, set [coref loss weight](https://github.com/dwadden/dygiepp/blob/master/training_config/scierc_working_example.jsonnet#L50) to 0 in the relevant training config.
- Split documents up into smaller chunks (5 sentences should be safe), make predictions using a model with coref prop, and stitch things back together.

See the [docs](https://allenai.github.io/allennlp-docs/api/commands/predict/) for more prediction options.



## Using it for the BioRelex Dataset

First you need to prepare the input data fro the DYGIEPP model.
Please us the prepare_input.py script for that.
```
python prepare_input.py --input [the data json] --output [path for saving scierc jsona file] 
--output_whitespaces [path for saving temprorary file necessary for recovering after prediction]
```

Then add the data paths in the scripts/train/train_biorelex.sh file and modify the training config in the G:\dygiepp\training_config/scierc_working_example.jsonnet file.

To train the model
```
bash ./scripts/train/train_biorelex.sh [gpu-id]
```
You can find the pre-trained model at this link -:
https://drive.google.com/file/d/14Lfase9AcaLoy37MdW6uAWDRRDWKPWlE/view?usp=drive_web

Now go the Re__biorelex_predictions folder

To Predict on the train and dev set., do the following

```
allennlp predict models/MODEL_NAME/model.tar.gz data/biorelex/processed_data/json/train.json --predictor dygie --include-package dygie --use-dataset-reader --output-file predictions/biorelex-train.jsonl --cuda-device 2
```

```
allennlp predict models/biorelex_sciie_bert_base_watch_rel_400_units_3_layers_co_coref_actula/model.tar.gz data/biorelex/processed_data/json/dev.json --predictor dygie --include-package dygie --use-dataset-reader --output-file predictions/biorelex-dev.jsonl --cuda-device 2
```

To convert the data to the format required by evaluate.py, do the following

```
python recover.py --prediction ../predictions/biorelex-train.jsonl --scierc_input train.json --whitespaces whitespaces_train.txt --output train_predictions.json
```

```
python recover.py --prediction ../predictions/biorelex-dev.jsonl --scierc_input dev.json --whitespaces whitespaces_dev.txt --output dev_predictions.json
```

To evaluate the file on the train and dev set, do the following

```
python evaluate.py --truth_path 1.0alpha7.train.json --prediction_path train_predictions.json --split train
```

```
python evaluate.py --truth_path 1.0alpha7.dev.json --prediction_path dev_predictions.json --split dev
```